// Establish the WebSocket connection and set up event handlers
const webSocket = new WebSocket("ws://a5413bc051dd.eu.ngrok.io/chat");
webSocket.onmessage = function (serverMessage) {
    // on we receive a message from the server, we simply display it
    alert(serverMessage.data);
};

// sends a message to the server and clears the input field
function send() {
	const messageElement = document.getElementById('message');
	const messageValue = messageElement.value;
	webSocket.send(messageValue);
	messageElement.value = "";
}