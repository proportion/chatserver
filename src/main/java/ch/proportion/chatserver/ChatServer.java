package ch.proportion.chatserver;

import static spark.Spark.*;

public class ChatServer {

  private ChatServer() {
  }

  public static void main(String[] args) {
    webSocket("/chat", ClientSocketHandler.class);
    port(80);
    init();
  }
}