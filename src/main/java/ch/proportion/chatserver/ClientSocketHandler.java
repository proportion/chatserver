package ch.proportion.chatserver;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.common.WebSocketSession;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

@WebSocket
public class ClientSocketHandler {

  private static final Logger LOG = Logger.getLogger(ChatServer.class.getName());

  // this map is shared between sessions and threads, so it needs to be thread-safe (http://stackoverflow.com/a/2688817)
  private static final Set<Session> SESSIONS = Collections.newSetFromMap(new ConcurrentHashMap<>());

  @OnWebSocketConnect
  public void onConnect(Session user) {
    LOG.info("client connected");
    SESSIONS.add(user);
  }

  @OnWebSocketClose
  public void onClose(Session user, int statusCode, String reason) {
    LOG.info("client disconnected");
    SESSIONS.remove(user);
  }

  @OnWebSocketMessage
  public void onMessage(Session user, String message) {
    SESSIONS.stream()
            .filter(Session::isOpen)
            .filter(session -> !session.equals(user))
            .filter(session -> session instanceof WebSocketSession)
            .map(session -> (WebSocketSession) session)
            .forEach(session -> {
              try {
                session.getRemote().sendString(message);
              } catch (IOException e) {
                throw new RuntimeException(e);
              }
            });
  }
}